export const search = async params => {
  const result = await fetch(
    `https://mock-air.herokuapp.com/search?departureStation=${params.from}&arrivalStation=${params.to}&date=${params.date}`,
    {
      method: "GET",
      headers: { "Content-type": "application/json" }
    }
  );
  return result.json();
};

export const listStations = async () => {
  const result = await fetch("https://mock-air.herokuapp.com/asset/stations", {
    method: "GET",
    headers: { "Content-type": "application/json" }
  });
  return result.json();
};
