import Vue from "vue";
import Vuex from "vuex";
import { listStations } from "../services";
import { search } from "../services";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    stations: [],
    searchedOutBoundFlight: [],
    searchedInBoundFlight: []
  },
  mutations: {
    getStations(state, result) {
      state.stations = result;
    },
    getSearchedOutBoundFlight(state, searchedFlights) {
      state.searchedOutBoundFlight = searchedFlights;
    },
    getSearchedInBoundFlight(state, searchedFlights) {
      state.searchedInBoundFlight = searchedFlights;
    }
  },
  actions: {
    async getStations(state) {
      const result = await listStations();
      state.commit("getStations", result);
    },
    async getSearchedOutBoundFlight(state, payload) {
      const searchedFlights = await search(payload);
      state.commit("getSearchedOutBoundFlight", searchedFlights);
    },
    async getSearchedInBoundFlight(state, payload) {
      const searchedFlights = await search(payload);
      state.commit("getSearchedInBoundFlight", searchedFlights);
    }
  }
});
