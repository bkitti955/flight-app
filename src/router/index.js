import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Flights from "../views/FlightSelect.vue";
import moment from "vue-moment";

Vue.use(VueRouter);
Vue.use(moment);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/flights",
    name: "Flights",
    component: Flights
  }
];

const router = new VueRouter({
  routes
});

export default router;
