export const outBoundMock = [
  {
    carrierCode: "W6",
    flightNumber: "5e2036fa82eb25ac7339675d",
    remainingTickets: 41,
    departure: "2020-02-12T00:30:00+0100",
    arrival: "2020-02-12T03:30:00+0100",
    fares: [
      { fareSellKey: "5e2036fa0d47cef313af84c0", price: 49, bundle: "basic" },
      {
        fareSellKey: "5e2036fa0d952adb71a6b090",
        price: 59,
        bundle: "standard"
      },
      { fareSellKey: "5e2036faf734f8ed72404d0d", price: 84, bundle: "plus" }
    ]
  },
  {
    carrierCode: "W6",
    flightNumber: "5e2036fa748bb57af2e692cf",
    remainingTickets: 31,
    departure: "2020-02-12T07:30:00+0100",
    arrival: "2020-02-12T10:30:00+0100",
    fares: [
      { fareSellKey: "5e2036fa502ec4402029f93d", price: 42, bundle: "basic" },
      {
        fareSellKey: "5e2036fa609972e1f0c6ba77",
        price: 49,
        bundle: "standard"
      },
      { fareSellKey: "5e2036fa1edfe6fc33c8b806", price: 79, bundle: "plus" }
    ]
  },
  {
    carrierCode: "W6",
    flightNumber: "5e2036fae7201c01db5b518a",
    remainingTickets: 44,
    departure: "2020-02-12T10:10:00+0100",
    arrival: "2020-02-12T13:10:00+0100",
    fares: [
      { fareSellKey: "5e2036fa22fa5e63e7e001d4", price: 51, bundle: "basic" },
      {
        fareSellKey: "5e2036fa2c2a74fc82130243",
        price: 58,
        bundle: "standard"
      },
      { fareSellKey: "5e2036fa4e0f53175a8c9dff", price: 88, bundle: "plus" }
    ]
  },
  {
    carrierCode: "W6",
    flightNumber: "5e2036fa153aca5be4565e1d",
    remainingTickets: 36,
    departure: "2020-02-12T13:20:00+0100",
    arrival: "2020-02-12T16:20:00+0100",
    fares: [
      { fareSellKey: "5e2036fa994e41ab26c19015", price: 72, bundle: "basic" },
      {
        fareSellKey: "5e2036fa6afb03cf45c74c5f",
        price: 81,
        bundle: "standard"
      },
      { fareSellKey: "5e2036faf44a00032030a594", price: 107, bundle: "plus" }
    ]
  }
];
